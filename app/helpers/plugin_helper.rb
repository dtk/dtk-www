module PluginHelper
  def PluginHelper.windows?
    (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end

  def PluginHelper.mac?
   (/darwin/ =~ RUBY_PLATFORM) != nil
  end

  def PluginHelper.unix?
    !PluginHelper.windows?
  end

  def PluginHelper.linux?
    PluginHelper.unix? and not PluginHelper.mac?
  end
end
