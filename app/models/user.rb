# user.rb ---
#
# Author: Julien Wintz
# Created: mar. avril  1 17:07:02 2014 (+0200)
# Version:
# Last-Updated:
#           By:
#     Update #: 2
#

# Change Log:
#
#

class User < ActiveRecord::Base
  devise :ldap_authenticatable, :recoverable, :rememberable, :trackable, :validatable
end
