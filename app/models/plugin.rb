# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

class Plugin
  attr_accessor :name
  attr_accessor :url
  attr_accessor :dependencies

  def initialize(name)
    @name = name
    @url = nil
    @dependencies = nil
  end
end

#
# plugin.rb ends here
