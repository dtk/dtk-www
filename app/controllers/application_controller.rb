# application_controller.rb ---
#
# Author: Julien Wintz
# Created: mer. avril  2 12:15:03 2014 (+0200)
# Version:
# Last-Updated:
#           By:
#     Update #: 6
#

# Change Log:
#
#

require 'digest/md5'

class ApplicationController < ActionController::Base

  rescue_from DeviseLdapAuthenticatable::LdapException do |exception|
    render :text => exception, :status => 500
  end

  protect_from_forgery with: :exception
end
