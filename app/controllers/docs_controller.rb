# docs_controller.rb ---
#
# Author: Julien Wintz
# Created: mer. avril  2 16:26:51 2014 (+0200)
# Version:
# Last-Updated:
#           By:
#     Update #: 306
#

# Change Log:
#
#

require 'nokogiri'
require 'open-uri'

class DocsController < ApplicationController

  def index
    @doc = fetch("dtkdoc/index").inner_html
  end

  def show
    @doc = fetch("#{params[:layer]}/#{params[:path]}").inner_html
  end

private

  def fetch(doc_path)
    doc_root = '/home/ci/dtk-doc'
    doc_file = File.open("#{doc_root}/#{doc_path}.html", "rt")
    doc_tree = Nokogiri::HTML(doc_file)
    doc_file.close

    doc_tree.xpath("/html/head").remove
    doc_tree.xpath("/html/body/div[@class='footer']").remove

## #################################################################
##
## #################################################################

    breadcrumb = doc_tree.xpath("/html/body//div[@class='navigationbar']/ul").first
    breadcrumb.name = 'ol'
    breadcrumb.set_attribute('class', 'breadcrumb');

## #################################################################
##
## #################################################################

    # doc_tree.xpath("/html/body//a[@href='dtk-allclasses.html']").each do |node|
    #   node['href'] = '/doc/dtkdoc/dtk-allclasses.html'
    # end
    # doc_tree.xpath("/html/body//a[@href='dtk-modules.html']").each do |node|
    #   node['href'] = '/doc/dtkdoc/dtk-modules.html'
    # end
    # doc_tree.xpath("/html/body//a[@href='dtk-guides.html']").each do |node|
    #   node['href'] = '/doc/dtkdoc/dtk-guides.html'
    # end
    # doc_tree.xpath("/html/body//a[@href='dtk-coding-style.html']").each do |node|
    #   node['href'] = '/doc/dtkdoc/dtk-coding-style.html'
    # end

## #################################################################
##
## #################################################################

    doc_tree.xpath("/html/body//div[@class='content mainContent']").each do |content|
      content['class'] = 'content content-doc'
      content.xpath("./h1[@class='title']").remove
      content.xpath("./span[@class='subtitle']").remove

      content.xpath(".//h3[@class='fn']").each do |content_member|
        content_member.name = 'pre'
      end
    end

## #################################################################
##
## #################################################################

    doc_tree.xpath("/html/body//table").wrap("<div class='table-responsive'></div>")
    doc_tree.xpath("/html/body//table").each do |table|
      table['class'] = 'table table-striped table-bordered table-doc'
      table.remove_attribute('width')
    end

## #################################################################
##
## #################################################################

    doc_tree.xpath("/html/body//div[@class='sectionlist normallist']").each do |list|

      list['class'] = 'panel panel-default panel-doc'

      list_title = list.xpath("./div[@class='heading']").first
      list_title['class'] = 'panel-heading'

      list_title_content = list_title.xpath("./h2").first
      list_title_referen = list_title.xpath("./a").first
      list_title_referen.content = list_title_content.content
      list_title_content.remove

      list_content = list.xpath("./div[@class='indexboxcont indexboxbar']")
      list_content_list = list_content.xpath("./ul").first
      list_content_list.parent = list
      list_content_list['class'] = 'list-group'
      list_content_list.xpath('./li').each do |list_content_item|
        list_content_item_ref = list_content_item.xpath('./a').first
        list_content_item.name = 'a';
        list_content_item.content = list_content_item_ref.content
        list_content_item['href'] = list_content_item_ref['href']
        list_content_item['class'] = 'list-group-item';
      end

      list_content.remove
    end

## #################################################################
##
## #################################################################

    doc_tree.xpath("/html/body//div[@class='landing']").each do |landing|
      landing_col1 = landing.xpath("./div[@class='col-1']").first
      landing_col2 = landing.xpath("./div[@class='col-2']").first
      landing_col2.add_next_sibling(landing_col1)
    end

## #################################################################
##
## #################################################################

    doc_tree.xpath("/html/body//div[@class='toc']").each do |panel|

      panel['class'] = 'panel panel-default panel-doc'

      panel_title = panel.xpath("./h3").first
      panel_title.name = 'div'
      panel_title['class'] = 'panel-heading'

      panel_content = panel.xpath("./ul").first
      panel_content['class'] = 'list-group'
      panel_content.xpath('./li').each do |panel_content_item|
        panel_content_item_ref = panel_content_item.xpath('./a').first
        panel_content_item.name = 'a';
        panel_content_item.content = panel_content_item_ref.content
        panel_content_item['href'] = panel_content_item_ref['href']
        panel_content_item['class'] = 'list-group-item';
      end

    end

## #################################################################

    return doc_tree
  end

end
