# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

class PluginController < ApplicationController

  def index
    @plugins = []

    Dir.foreach(Rails.root.join('public' + '/' + 'plugins')) do |item|
      next if item == '.' or item == '..' or item == '.gitkeep'
      @plugins << Plugin.new(item)
    end
  end

  def show
    @file = Rails.root.join('public' + '/' + 'plugins' + '/' + params[:id])

    @plugin = nil

    Dir.foreach(Rails.root.join('public/plugins')) do |item|
      next if item == '.' or item == '..' or item == '.gitkeep'
      if(item.include? params[:id])
        @plugin = Plugin.new(item)
        @plugin.url = '/plugins/' + item
        if PluginHelper.mac?
          @plugin.dependencies = %x(otool -L #{Rails.root.join('public/plugins/' + item)})
        end
      end
    end
  end
end

#
# plugin_controller.rb ends here
